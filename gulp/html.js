exports.handleHtml = (gulp, buildFolder, browserSync) => {
    gulp.task("html", () => {
        return gulp
            .src("../src/html/*.html")
            .pipe(gulp.dest(buildFolder))
            .pipe(browserSync.stream());
    });
};
