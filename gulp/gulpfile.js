const gulp = require("gulp");
const nodemon = require("gulp-nodemon");
const path = require("path");
const browserSync = require("browser-sync").create({});

const baseDir = path.join(__dirname, "..");
const buildFolder = path.join(baseDir, "build");

const assets = require("./assets");
assets.handleAssets(gulp, buildFolder, browserSync);

const css = require("./css");
css.handleCss(gulp, buildFolder, browserSync);

const ts = require("./typescript");
ts.handleTs(gulp, buildFolder, browserSync);

const html = require("./html");
html.handleHtml(gulp, buildFolder, browserSync);

gulp.task("reload", function () {
    return browserSync.reload({ stream: true });
});

function initBrowser() {
    browserSync.init({
        server: buildFolder,
        port: 3005,
        logLevel: "info",
        logPrefix: "BS",
        online: false,
        xip: false,
        notify: false,
        reloadDebounce: 100,
        reloadOnRestart: true,
        watchEvents: ["add", "change"],
    });
}

gulp.task("dev", gulp.series("scripts", "img", "styles", "html", "other"));

gulp.task(
    "watch",
    gulp.series("dev", function () {
        initBrowser();
        gulp.watch("../src/css/**/*.scss", gulp.series("styles"));
        gulp.watch("../src/js/**/*.ts", gulp.series("scripts"));
        gulp.watch("../res/**/*.{png,jpe?g,gif}", gulp.series("img"));
        gulp.watch("../src/html/*.html", gulp.series("html"));
        gulp.watch("../res/**/*.woff2", gulp.series("other"));
    })
);

gulp.task("default", gulp.series("dev"));
