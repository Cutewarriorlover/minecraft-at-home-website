const spritesmith = require("gulp.spritesmith");
const merge = require("merge-stream");
const imagemin = require("gulp-imagemin");
const buffer = require("vinyl-buffer");

exports.handleAssets = (gulp, buildFolder) => {
    gulp.task("img", function () {
        const spriteData = gulp.src("../res/**/*.{png,jpe?g,gif}").pipe(
            spritesmith({
                imgName: "sprite.png",
                cssName: "_sprite.scss",
                imgPath: "res/sprite.png",
            })
        );

        const imgStream = spriteData.img
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest(buildFolder + "/res"));
        const cssStream = spriteData.css.pipe(gulp.dest("../src/css"));

        return merge(imgStream, cssStream);
    });

    gulp.task("other", function () {
        return gulp
            .src("../src/fonts/**/*.{woff2,woff,ttf,eot,svg}")
            .pipe(gulp.dest(buildFolder + "/res"));
    });
};
