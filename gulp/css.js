const dartSass = require("gulp-dart-sass");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const postcssCriticalSplit = require("postcss-critical-split");
const sourcemaps = require("gulp-sourcemaps");

exports.handleCss = function (gulp, buildFolder, browserSync) {
    gulp.task("styles", function () {
        return gulp
            .src("../src/css/main.scss", { cwd: __dirname })
            .pipe(plumber())
            .pipe(sourcemaps.init())
            .pipe(dartSass.sync().on("error", dartSass.logError))
            .pipe(
                postcss([
                    postcssCriticalSplit({
                        blockTag: "@load-async",
                        output: "rest",
                    }),
                ])
            )
            .pipe(postcss([require("autoprefixer"), require("cssnano")]))
            .pipe(sourcemaps.write("."))
            .pipe(gulp.dest(buildFolder))
            .pipe(browserSync.stream());
    });
};
