const glob = require("glob");
const execSync = require("child_process").execSync;
const trim = require("trim");
const fs = require("fs");
const path = require("path");

module.exports = {
    getRevision(useLast = false) {
        const commitHash = execSync(
            "git rev-parse --short " + (useLast ? "HEAD^1" : "HEAD")
        ).toString("ascii");
        return commitHash.replace(/^\s+|\s+$/g, "");
    },

    getVersion() {
        return trim(
            fs.readFileSync(path.join(__dirname, "..", "version")).toString()
        );
    },
};
